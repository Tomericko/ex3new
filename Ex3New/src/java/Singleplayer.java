
import java.io.IOException;
import java.util.HashMap;
import mainPackage.AppConfigurations;
import mainPackage.WebServer;
import mainPackage.Maze;
import mainPackage.Position;
import org.json.JSONObject;

/**
 * class for mange the connection with the server for generating and updating
 * the singleplayer game.
 */
public class Singleplayer {

    private WebServer tcp;
    private String mazeName;
    private Maze maze;

    /**
     * constructor that get the singleplayer name.
     *
     * @param mazeName the singleplayer name
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public Singleplayer(String mazeName) throws IOException, ClassNotFoundException {
        tcp = new WebServer();
        this.mazeName = mazeName;
    }

    /**
     * get the maze member from this class.
     *
     * @return the maze member.
     */
    public Maze getMaze() {
        return this.maze;
    }

    /**
     * get the singleplayer answer from the server and parse it to hash map.
     *
     * @return the parsed answer.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public HashMap<String, Object> getSingleplayer() throws IOException, ClassNotFoundException {

        int height = Integer.parseInt(AppConfigurations.getInstance().getConfiguration("height"));
        int width = Integer.parseInt(AppConfigurations.getInstance().getConfiguration("width"));

        tcp.SendToServer("generate " + this.mazeName + " 0 " + String.valueOf(height) + " " + String.valueOf(width));

        String ans = this.tcp.GetAnswer();
        this.tcp.CloseConnection();
        JSONObject obj = new JSONObject(ans);
        JSONObject objContent = new JSONObject(obj.getString("Content"));
        String mazes = objContent.getString("Maze");
        JSONObject objStart = objContent.getJSONObject("Start");
        JSONObject objEnd = objContent.getJSONObject("End");

        Maze maze = new Maze();
        Position end = new Position();
        end.setRow(objEnd.getInt("Row"));
        end.setCol(objEnd.getInt("Col"));
        Position entrance = new Position();
        entrance.setRow(objStart.getInt("Row"));
        entrance.setCol(objStart.getInt("Col"));
        maze.setEnd(end);
        maze.setEntrance(entrance);
        maze.setMazeName(objContent.getString("name"));
        maze.setMazeString(mazes);
        maze.setHeight(height);
        maze.setWidth(width);

        HashMap<String, Object> answer = new HashMap<String, Object>();
        answer.put("maze", maze);
        answer.put("height", height);
        answer.put("width", width);
        
        this.maze = maze;
        return answer;
    }

}
