import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 * servlet for generate multiplayer game asynchrozied.
 */
@WebServlet(urlPatterns = {"/secured/MultiPlayerLongPolling"}, asyncSupported = true)
public class MultiPlayerLongPolling extends HttpServlet {

    private String mazeName;
    private HashMap<String, Object> maze;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * connect to server and get a new multiplayer game.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException,
            IOException {
        try {
            mazeName = (String) req.getSession().getAttribute("mazeName");
            maze = new HashMap<String, Object>();
            Multiplayer multi = new Multiplayer(mazeName);
            req.getSession().setAttribute("multi", multi);
            maze = multi.getMultiplayer();
            JSONObject obj = new JSONObject();
            obj.put("answer", maze);//put the answer from the server in the json 
            resp.getWriter().write(obj.toString());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SinglePlayerLongPolling.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(MultiPlayerLongPolling.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
