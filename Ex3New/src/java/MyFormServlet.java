
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import mainPackage.DBHandler;
import mainPackage.User;

@WebServlet(name = "MyFormServlet", urlPatterns = {"/MyFormServlet"}, asyncSupported = true)
public class MyFormServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("login") != null) {
            try {
                String password = request.getParameter("password");
                String userName = request.getParameter("username");
                User user = DBHandler.getInstance().getUser(userName);
                if ((user != null) && (user.getPassword().equals(password))) {
                    HttpSession session = request.getSession();
                    session.setAttribute("username", user.getUserName());
                    session.setAttribute("icon", user.getIcon());
                    session.setAttribute("realname", user.getRealName());
                    response.sendRedirect("secured/MainMenu.jsp");
                } else {
                    request.setAttribute("error", true);
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(MyFormServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (request.getParameter("signup") != null) {
            request.setAttribute("error", false);
            response.sendRedirect("Register.jsp");
        }
    }
}
