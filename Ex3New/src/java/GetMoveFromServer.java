
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;
import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 * a runnable class that get move from server asynchronized
 * on the multiplayer game.
 * @author user
 */
public class GetMoveFromServer implements Runnable {

    AsyncContext asyncContext;
    Random random;
    private HashMap<String, Object> move;
    Multiplayer multi;

    /**
     * constructor thet get sasync context and mltiplayer game to update
     * the mhe move from the server.
     * @param asyncContext
     * @param multi 
     */
    public GetMoveFromServer(AsyncContext asyncContext, Multiplayer multi) {
        this.multi = multi;
        this.asyncContext = asyncContext;
        move = new HashMap<String, Object>();
    }

    
    /**
     * override function that wating for the answer from the server.
     */
    @Override
    public void run() {
        try {
            if (asyncContext != null) {
                move = multi.getOpMove();
                JSONObject obj = new JSONObject();
                obj.put("answer", move);//put the answer from the server in the json object
                HttpServletResponse peer = (HttpServletResponse) asyncContext.getResponse();
                peer.getWriter().write(obj.toString());
                peer.setStatus(HttpServletResponse.SC_OK);
                peer.setContentType("application/json");
                asyncContext.complete();
            }
        } catch (IOException e) {
        }
    }

}
