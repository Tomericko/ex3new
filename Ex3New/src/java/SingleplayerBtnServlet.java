/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 * servlet for get the singleplayer maze asynchronized.
 */
@WebServlet(name = "SingleplayerBtnServlet", urlPatterns = {"/secured/SingleplayerBtnServlet"}, asyncSupported = true)
public class SingleplayerBtnServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods. the page send to this servlet when the user click on one of the
     * game buttons, this servlet check what button was clicked and act
     * accordigly.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException {
        if (request.getParameter("type").equals("start_over")) {
            JSONObject js = new JSONObject();
            Singleplayer single = (Singleplayer) request.getSession(false).getAttribute("single");
            //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~TO DO
            js.put("Position", single.getMaze().getEntrance().getRow() * 2 * 29 + single.getMaze().getEntrance().getCol() * 2);

            response.getWriter().write(js.toString());
        } else if (request.getParameter("type").equals("close")) {
            Multiplayer multi = (Multiplayer) request.getSession().getAttribute("multi");
            multi.closeConnection();
        } else if (request.getParameter("type").equals("clue")) {
            Clue clue = new Clue();
            String gameType = (String) (request.getParameter("gameType"));
            String s = (String) (request.getParameter("clueBtn"));
            int position = Integer.parseInt(s);

            String name = (String) request.getSession().getAttribute("mazeName");
            String[] answer = null;
            if (gameType.equals("multi")) {
                Multiplayer multi = (Multiplayer) request.getSession().getAttribute("multi");
                name += "_maze";
                int y = ((position) / ((multi.getMyMaze().getHeight() * 2) - 1)) / 2;
                int x = ((position) % ((multi.getMyMaze().getWidth() * 2) - 1)) / 2;
                answer = clue.getClue(name, x, y, multi.getMyMaze().getHeight(), multi.getMyMaze().getWidth());

            } else {
                Singleplayer single = (Singleplayer) request.getSession().getAttribute("single");
                int y = ((position) / ((single.getMaze().getHeight() * 2) - 1)) / 2;
                int x = ((position) % ((single.getMaze().getWidth() * 2) - 1)) / 2;
                answer = clue.getClue(name, x, y, single.getMaze().getHeight(), single.getMaze().getWidth());

            }
                JSONObject js = new JSONObject();
                js.put("newClue", answer[0]);
                js.put("direction", answer[1]);
                response.getWriter().write(js.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SingleplayerBtnServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SingleplayerBtnServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
