
import java.io.IOException;
import java.util.HashMap;
import java.util.Random;
import mainPackage.AppConfigurations;
import mainPackage.WebServer;
import mainPackage.Maze;
import mainPackage.Position;
import org.json.JSONObject;



/**
 * class for mange the connection with the server for generating and updating
 * the multiplayer game.
 */
public class Multiplayer {

    private WebServer tcp;
    private String mazeName;
    private Maze myMaze;
    private Maze opMaze;

    /**
     * constructor that get the multiplayer name.
     * 
     * @param mazeName the multiplayer name
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public Multiplayer(String mazeName) throws IOException, ClassNotFoundException {
        tcp = new WebServer();
        this.mazeName = mazeName;
    }

    /**
     * get the multiplayer answer from the server and parse it to hash map.
     *
     * @return the parsed answer.
     * @throws IOException
     * @throws InterruptedException
     */
    public HashMap<String, Object> getMultiplayer() throws IOException, InterruptedException, ClassNotFoundException {

        int height = Integer.parseInt(AppConfigurations.getInstance().getConfiguration("height"));
        int width = Integer.parseInt(AppConfigurations.getInstance().getConfiguration("width"));

        tcp.SendToServer("multiplayer " + this.getMazeName() + " " + String.valueOf(height) + " " + String.valueOf(width));
        String ans = this.tcp.GetAnswer();
        Random r = new Random();
        Thread.sleep(r.nextInt(2000));
        JSONObject obj = new JSONObject(ans);
        JSONObject objContent = new JSONObject(obj.getString("Content"));

        JSONObject myMazeJson = objContent.getJSONObject("You");
        JSONObject opMazeJson = objContent.getJSONObject("Other");

        JSONObject myObjStart = myMazeJson.getJSONObject("Start");
        JSONObject myObjEnd = myMazeJson.getJSONObject("End");

        JSONObject opObjStart = opMazeJson.getJSONObject("Start");
        JSONObject opObjEnd = opMazeJson.getJSONObject("End");

        String myMazeString = myMazeJson.getString("Maze");
        String opMazeString = opMazeJson.getString("Maze");

        this.myMaze = new Maze();
        this.opMaze = new Maze();

        //parse my maze
        Position myEnd = new Position();
        myEnd.setRow(myObjEnd.getInt("Row"));
        myEnd.setCol(myObjEnd.getInt("Col"));
        Position myEntrance = new Position();
        myEntrance.setRow(myObjStart.getInt("Row"));
        myEntrance.setCol(myObjStart.getInt("Col"));
        getMyMaze().setEnd(myEnd);
        getMyMaze().setEntrance(myEntrance);
        getMyMaze().setMazeName(objContent.getString("Maze"));
        getMyMaze().setMazeString(myMazeString);
        getMyMaze().setHeight(height);
        getMyMaze().setWidth(width);

        //parse my maze
        Position opEnd = new Position();
        Position opEntrence = new Position();
        opEnd.setRow(opObjEnd.getInt("Row"));
        opEnd.setCol(opObjEnd.getInt("Col"));
        opEntrence.setRow(opObjStart.getInt("Row"));
        opEntrence.setCol(opObjStart.getInt("Col"));
        getOpMaze().setEnd(opEnd);
        getOpMaze().setEntrance(opEntrence);
        getOpMaze().setMazeName(objContent.getString("Maze"));
        getOpMaze().setMazeString(opMazeString);
        getOpMaze().setHeight(height);
        getOpMaze().setWidth(width);

        HashMap<String, Object> answer = new HashMap<String, Object>();
        answer.put("opMaze", getOpMaze());
        answer.put("myMaze", getMyMaze());
        answer.put("height", height);
        answer.put("width", width);
        return answer;
    }

    public HashMap<String, Object> getOpMove() throws IOException {

        String ans = this.tcp.GetAnswer();

        JSONObject obj = new JSONObject(ans);
        HashMap<String, Object> answer = new HashMap<String, Object>();
        if(obj.getString("Type").equals("5")){
            answer.put("mazeName", this.getMazeName());
            answer.put("Move", "close");
        }else{
            JSONObject objContent = new JSONObject(obj.getString("Content"));
            String move = objContent.getString("Move");
            answer.put("mazeName", this.getMazeName());
            answer.put("Move", move);
        }
        return answer;

    }

    public void sendMove(String direction) throws IOException {
        this.tcp.SendToServer("play " + direction);
    }

    public void closeConnection() throws IOException {
        this.tcp.SendToServer("close "+ this.getMazeName());
    }

    /**
     * @return the mazeName
     */
    public String getMazeName() {
        return mazeName;
    }

    /**
     * @return the myMaze
     */
    public Maze getMyMaze() {
        return myMaze;
    }

    /**
     * @return the opMaze
     */
    public Maze getOpMaze() {
        return opMaze;
    }
}
