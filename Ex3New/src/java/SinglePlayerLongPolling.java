import java.io.IOException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author tomer
 */
@WebServlet(urlPatterns = {"/secured/SinglePlayerLongPolling"}, asyncSupported = true)
public final class SinglePlayerLongPolling extends HttpServlet {


    private String mazeName;
    private HashMap<String, Object> maze;

    @Override
    protected void doGet(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException,
            IOException {
        try {
            this.mazeName = (String) req.getSession().getAttribute("mazeName");
            maze = new HashMap<String, Object>();
            Singleplayer single = new Singleplayer(mazeName);
            req.getSession().setAttribute("single", single);
            maze = single.getSingleplayer();
            JSONObject obj = new JSONObject();
            obj.put("answer", maze);//put the answer from the server in the json object
            resp.getWriter().write(obj.toString());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SinglePlayerLongPolling.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
