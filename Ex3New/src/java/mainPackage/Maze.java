/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainPackage;

import java.io.Serializable;

/**
 *
 * @author user
 */
public class Maze implements Serializable {

    private String mazeName;
    private String mazeString;
    private Position entrance;
    private Position end;
    private Integer height;
    private Integer width;

    public Maze() {
        mazeName = new String();
        mazeString = new String();
        entrance = new Position();
        end = new Position();
    }

    public String getMazeName() {
        return mazeName;
    }

    public String getMazeString() {
        return mazeString;
    }

    public Position getEntrance() {
        return entrance;
    }

    public Position getEnd() {
        return end;
    }

    public void setMazeName(String mazeName) {
        this.mazeName = mazeName;
    }

    public void setMazeString(String mazeString) {
        this.mazeString = mazeString;
    }

    public void setEntrance(Position entrance) {
        this.entrance = entrance;
    }

    public void setEnd(Position end) {
        this.end = end;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }
}
