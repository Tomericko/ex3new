/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public class DBHandler {

    private static DBHandler instance = null;
    private static Object mutex = new Object();

    private ArrayList<User> users;
    private ArrayList<Maze> mazes;

    private DBHandler() throws IOException, ClassNotFoundException {
        loadMazes();
        loadUsers();
    }

    public static DBHandler getInstance() throws IOException, ClassNotFoundException {
        if (instance == null) {
            synchronized (mutex) {
                if (instance == null) {
                    instance = new DBHandler();
                }
            }
        }
        return instance;
    }

    public void loadUsers() throws IOException, ClassNotFoundException {
        File file = new File("users.txt");
        if (file.exists() && !file.isDirectory() && file.length() != 0) {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            users = (ArrayList<User>) ois.readObject();
            ois.close();
        } else {
            file.createNewFile();
            users = new ArrayList<User>();
        }
    }

    public void saveUsers() throws IOException, ClassNotFoundException {
        File file = new File("users.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fis = new FileOutputStream(file);
        fis.getChannel().truncate(0);
        ObjectOutputStream ois = new ObjectOutputStream(fis);
        ois.writeObject(users);
        ois.close();
        fis.close();
    }

    public void loadMazes() throws IOException, ClassNotFoundException {
        File file = new File("mazes.txt");
        if (file.exists() && !file.isDirectory() && file.length() != 0) {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            mazes = (ArrayList<Maze>) ois.readObject();
            ois.close();
        } else {
            file.createNewFile();
            mazes = new ArrayList<Maze>();
        }
    }

    public void saveMazes() throws IOException, ClassNotFoundException {
        File file = new File("mazes.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fis = new FileOutputStream(file);
        fis.getChannel().truncate(0);
        ObjectOutputStream ois = new ObjectOutputStream(fis);
        ois.writeObject(mazes);
        ois.close();
        fis.close();
    }

    public User getUser(String userName) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getUserName().equals(userName)) {
                return users.get(i);
            }
        }
        return null;
    }

    public Maze getMaze(String mazeName) {
        for (int i = 0; i < mazes.size(); i++) {
            if (mazes.get(i).getMazeName().equals(mazeName)) {
                return mazes.get(i);
            }
        }
        return null;
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public void addMaze(Maze maze) {
        this.mazes.add(maze);
    }
    
    public void removeUser(User user) {
        this.users.remove(user);
    }

    public void removeMaze(Maze maze) {
        this.mazes.remove(maze);
    }
}
