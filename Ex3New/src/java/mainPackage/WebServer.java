package mainPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;


/**
 *
 * @author user
 */
public class WebServer {

    private final Socket socket;
    private final PrintWriter out;
    private final BufferedReader in;
    private final String ip;
    private final int port;
    private Object sendMutex = new Object();
    private Object recvMutex = new Object();

    public WebServer() throws IOException, ClassNotFoundException {
        ip = AppConfigurations.getInstance().getConfiguration("ip");
        port = Integer.parseInt(AppConfigurations.getInstance().getConfiguration("port"));
        socket = new Socket(ip, port);
        this.out = new PrintWriter(socket.getOutputStream(), true);
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public void SendToServer(String message) {
        synchronized (sendMutex) {
            out.println(message);
        }
    }

    public String GetAnswer() throws IOException {
        try {
            Files.write(Paths.get("sys.txt"), "1\r\n".getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
        synchronized (recvMutex) {
            try {
                Files.write(Paths.get("sys.txt"), "2\r\n".getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                //exception handling left as an exercise for the reader
            }
            StringBuilder result = new StringBuilder();
            char[] cur = new char[16384];
            int num;
            if ((num = in.read(cur)) != -1) {
                result.append(new String(cur, 0, num));
            }
            try {
                Files.write(Paths.get("sys.txt"), "3\r\n".getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                //exception handling left as an exercise for the reader
            }
            return result.toString();
        }
    }

    public void CloseConnection() throws IOException {
        this.socket.close();
    }
}
