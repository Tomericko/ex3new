package mainPackage;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * static class for manging the application configurations.
 */
public class AppConfigurations {

    private static AppConfigurations instance = null;
    private static Object mutex = new Object();
    private Properties properties;

    /**
     * private constructor that get the 
     * @throws IOException 
     */
    private AppConfigurations() throws IOException {
        properties = new Properties();
        File configFile = new File("config.properties");
        if (!configFile.exists()) {
            configFile.createNewFile();
        }
        FileReader reader = new FileReader(configFile);
        properties.load(reader);
        String ip = properties.getProperty("ip");
        String port = properties.getProperty("port");
        String height = properties.getProperty("height");
        String width = properties.getProperty("width");
        if ((ip == null) || (port == null) || (height == null) || (width == null)) {
            if (ip == null) {
                properties.setProperty("ip", "127.0.0.1");
            }
            if (port == null) {
                properties.setProperty("port", "35555");
            }
            if (height == null) {
                properties.setProperty("height", "15");
            }
            if (width == null) {
                properties.setProperty("width", "15");
            }
            FileWriter writer = new FileWriter(configFile);
            properties.store(writer, "App configurations:");
        }
    }

    public static AppConfigurations getInstance() throws IOException, ClassNotFoundException {
        if (instance == null) {
            synchronized (mutex) {
                if (instance == null) {
                    instance = new AppConfigurations();
                }
            }
        }
        return instance;
    }

    public String getConfiguration(String configuration) throws IOException, ClassNotFoundException {
        File configFile = new File("config.properties");
        FileReader reader = new FileReader(configFile);
        properties.load(reader);
        String configurationValue = properties.getProperty(configuration);
        reader.close();
        return configurationValue;

    }

    public void setConfiguration(String key, String value) throws IOException, ClassNotFoundException {
        File configFile = new File("config.properties");
        properties.setProperty(key, value);
        FileWriter writer = new FileWriter(configFile);
        properties.store(writer, "App configurations:");
        writer.close();
    }
}
