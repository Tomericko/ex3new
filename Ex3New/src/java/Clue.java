
import java.io.IOException;
import mainPackage.Position;
import mainPackage.WebServer;
import org.json.JSONObject;

/**
 * class for sending the server the player position and get the clue.
 */
public class Clue  {
    private WebServer tcp;
    
    /**
     * constructor which make a new connection to the server for getting a clue.
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public Clue() throws IOException, ClassNotFoundException{
        this.tcp = new WebServer();
    }
    
    /**
     * this function send the server the player position and get the next step.
     * @param mazeName the maze name as string.
     * @param positionX the x coordinate of the player position.
     * @param positionY the y coordinate of the player position.
     * @param width the width of the maze.
     * @param height the height of the maze.
     * @return the next step.
     * @throws IOException 
     */
    public String[] getClue(String mazeName, int positionX, int positionY, int width, int height) throws IOException{
        String fullAnswer[] = new String[2];
        Position playerPosition = new Position(positionX,positionY);
        String direction = "";
        
        //get the solution from the player position from the server.
        this.tcp.SendToServer("solve "+mazeName+" 0 "+ Integer.toString(positionX) + " "+Integer.toString(positionY));
        String answer = this.tcp.GetAnswer();
        
        Position closest = new Position(-1,-1);
        JSONObject jo = new JSONObject(answer);
        
        //deserialize the server answer.
        String jo2 = (String)jo.getString("Content");
        JSONObject j = new JSONObject(jo2);
        String solution = (String)j.get("Maze");
        int middle = 0;
        int index = playerPosition.getRow()*2*(width*2 - 1) + playerPosition.getCol()*2;
        //get the next step from the server answer.
        if(index < solution.length() - 1 && solution.charAt(index+1)=='2'
                && solution.charAt(index+2) == '2'){
            //right
            direction = "right";
            middle = -1;
            closest = new Position(playerPosition.getCol()+1,playerPosition.getRow());
        }else if(index>1 && solution.charAt(index-2) == '2' &&
                solution.charAt(index-1)=='2'){
            //left
            direction = "left";
            middle = 1;
            closest = new Position(playerPosition.getCol()-1,playerPosition.getRow());
        }else if(index+width*2*2<solution.length()+1 &&
                solution.charAt(index+width*2 - 1)=='2'&&
                solution.charAt(index+(width*2 - 1)*2) == '2'){
            //down
            direction = "down";
            middle = (-1)*((width*2) - 1);
            closest = new Position(playerPosition.getCol(),playerPosition.getRow()+1);
        }else if(index-width*2*2>0 &&
                solution.charAt(index-(width*2 - 1))=='2'&&
                solution.charAt(index-(width*2 - 1)*2) == '2'){
            //up
            direction = "up";
            middle = (width*2) - 1;
            closest = new Position(playerPosition.getCol(),playerPosition.getRow()-1);
        }
        
        fullAnswer[0] = Integer.toString((closest.getCol()*2+closest.getRow()*2*(2*width-1))+middle);
        fullAnswer[1] = direction;
        return fullAnswer;
        
    }

}
