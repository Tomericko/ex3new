import java.io.IOException;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * servlet for get the move from the server via long polling.
 */
@WebServlet(name = "MoveLongPolling", urlPatterns = {"/secured/MoveLongPolling"}, asyncSupported = true)
public class MoveLongPolling extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * start new thread that get the async context and the multiplayer game and
     * waiting for the server to send thr opponent move.
     *
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req,
            HttpServletResponse resp) throws ServletException,
            IOException {
        Multiplayer multi = (Multiplayer) req.getSession().getAttribute("multi");
        AsyncContext async = req.startAsync();
        async.setTimeout(0);
        Thread generator = new Thread(new GetMoveFromServer(async, multi));
        generator.start();
    }
}
