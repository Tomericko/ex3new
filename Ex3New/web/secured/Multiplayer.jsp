
<%@page import="mainPackage.Maze" %>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="SingleplayerCSS.css">
        <title>Multiplayer</title>     
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="MultiplayerJS.js"></script>
    </head>
    <body>
        <%request.getSession().setAttribute("type", "multiplayer");%>
        <div id="toolBar">
            <label style="font-size: 1.5vw;">Welcome <%=(String) request.getSession().getAttribute("username")%>!</label><br/>
            <img id="user_icon" src="<%=(String) request.getSession().getAttribute("icon")%>" align="middle"><br/>
            <a href="/secured/LogoutServlet" onclick="logout()" style="font-size: 1.5vw;" name="logout" id="logout_btn" style="font-size: 100%">Logout</a>
        </div>
        <table>
            <tr>
                <td><button id="clueBtnMulti" name="clueBtnMulti" title="hint"></button></td>
                <td> <button id="start_overBtn" name="start_overBtn" title="start over"></button></td>
                <td><button id="exitBtn" name="exitBtn" title="escape"></button></td>
            </tr>
        </table>
            <img src="resources/maze/mr_potato_head_heavy.gif" alt="Loading..." id="loadingImg" class="center">

        <script type="text/javascript">
            $(function ($) {
                function long_polling() {
                    $.getJSON('MultiPlayerLongPolling', function (data) {
                        $("#loadingImg").css("display", "none");
                            var rows = data.answer.height * 2 - 1;
                            var cols = data.answer.width * 2 - 1;
                            var myMaze = data.answer.myMaze.mazeString.replaceAt((data.answer.myMaze.entrance.row * 2) * (rows) + data.answer.myMaze.entrance.col * 2, '2');
                            myMaze = myMaze.replaceAt((data.answer.myMaze.end.row * 2) * (rows) + data.answer.myMaze.end.col * 2, '3');

                            var opMaze = data.answer.opMaze.mazeString.replaceAt((data.answer.opMaze.entrance.row * 2) * (rows) + data.answer.opMaze.entrance.col * 2, '2');
                            opMaze = opMaze.replaceAt((data.answer.opMaze.end.row * 2) * (rows) + data.answer.opMaze.end.col * 2, '3');

                            var blockImage = "resources/maze/Brick_Block.png";
                            var borderImage = "resources/maze/Used_Block.png";
                            var playerImage = "resources/maze/player.png";
                            var exitImage = "resources/maze/exit5.png";
                            var playerHeight = 500.0 / rows + 2;
                            var playerWidth = 850.0 / cols + 2;
                            var closeHeight = 600.0 / rows + 2;
                            var closeWidth = 850.0 / cols + 2;
                            var borderHeight = 600.0 / rows + 2;
                            var borderWidth = 850.0 / cols + 2;

                            var main = document.createElement("table");
                            main.setAttribute("id", "mainTable");
                            var firstRow = document.createElement("tr");
                            var firstData = document.createElement("td");
                         
                            var secondData = document.createElement("td");
                            var div = document.createElement("div");
                            var game = document.createElement("table");
                            game.setAttribute("id", "gameBoard");

                            var position = 0;
                            var i =0, j=0;
                            var tempImg;
                            var tempTd;
                            var tempTr;
                            for (i = 0; i < rows + 2; i++) {
                                tempTr = document.createElement("tr");
                                for (j = 0; j < cols + 2; j++) {
                                    tempTd = document.createElement("td");
                                    tempImg = document.createElement("img");
                                    if (i === 0 || i === cols + 1 || j === 0 || j === rows + 1) {
                                        tempImg.setAttribute("id", "close");
                                        tempImg.setAttribute("src", borderImage);
                                        tempImg.setAttribute("height", borderHeight);
                                        tempImg.setAttribute("width", borderWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                    } else if (myMaze[position] === '1') {
                                        tempTd.setAttribute("id", "border");
                                        tempImg.setAttribute("id", position);
                                        tempImg.setAttribute("src", blockImage);
                                        tempImg.setAttribute("height", closeHeight);
                                        tempImg.setAttribute("width", closeWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        position++;
                                    } else if (myMaze[position] === '2') {
                                        tempTd.setAttribute("id", "player");
                                        tempImg.setAttribute("id", position);
                                        tempImg.setAttribute("src", playerImage);
                                        tempImg.setAttribute("height", playerHeight);
                                        tempImg.setAttribute("width", playerWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        $("#clueBtnMulti").attr("value", position);
                                        position++;
                                    } else if (myMaze[position] === '3') {
                                        tempTd.setAttribute("id", "end");
                                        tempImg.setAttribute("id", position);
                                        tempImg.setAttribute("src", exitImage);
                                        tempImg.setAttribute("height", closeHeight);
                                        tempImg.setAttribute("width", closeWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        position++;
                                    } else if (myMaze[position] === '0') {
                                        tempTd.setAttribute("id", "open");
                                        tempImg.setAttribute("id", position);
                                        tempImg.setAttribute("src", playerImage);
                                        tempImg.setAttribute("height", closeHeight);
                                        tempImg.setAttribute("width", closeWidth);
                                        tempImg.setAttribute("class", "mazeBlock invisible");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        position++;
                                    }
                                    tempTd.appendChild(tempImg);
                                    tempTr.appendChild(tempTd);
                                }
                                game.appendChild(tempTr);
                            }

                            div.setAttribute("class", "container");
                            div.appendChild(game);
                            secondData.appendChild(div);
                            firstRow.appendChild(secondData);

                            /////////////////
                            var tempData;
                            tempData = document.createElement("td");

                            firstRow.appendChild(tempData);

                            var thirdData = document.createElement("td");
                            div = document.createElement("div");
                            game = document.createElement("table");
                            game.setAttribute("id", "gameBoard");

                            var position = 0;
                            var tempImg;
                            var tempTd;
                            var tempTr;
                            for (i = 0; i < rows + 2; i++) {
                                tempTr = document.createElement("tr");
                                for (j = 0; j < cols + 2; j++) {
                                    tempTd = document.createElement("td");
                                    tempImg = document.createElement("img");
                                    if (i === 0 || i === cols + 1 || j === 0 || j === rows + 1) {
                                        tempImg.setAttribute("id", "close");
                                        tempImg.setAttribute("src", borderImage);
                                        tempImg.setAttribute("height", borderHeight);
                                        tempImg.setAttribute("width", borderWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                    } else if (opMaze[position] === '1') {
                                        tempTd.setAttribute("id", "border");
                                        tempImg.setAttribute("id", "o_"+position);
                                        tempImg.setAttribute("src", blockImage);
                                        tempImg.setAttribute("height", closeHeight);
                                        tempImg.setAttribute("width", closeWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        position++;
                                    } else if (opMaze[position] === '2') {
                                        tempTd.setAttribute("id", "opPlayer");
                                        tempImg.setAttribute("id", "o_"+position);
                                        tempImg.setAttribute("src", playerImage);
                                        tempImg.setAttribute("height", playerHeight);
                                        tempImg.setAttribute("width", playerWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        position++;
                                    } else if (opMaze[position] === '3') {
                                        tempTd.setAttribute("id", "end");
                                        tempImg.setAttribute("id", "o_"+position);
                                        tempImg.setAttribute("src", exitImage);
                                        tempImg.setAttribute("height", closeHeight);
                                        tempImg.setAttribute("width", closeWidth);
                                        tempImg.setAttribute("class", "mazeBlock");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        position++;
                                    } else if (opMaze[position] === '0') {
                                        tempTd.setAttribute("id", "open");
                                        tempImg.setAttribute("id", "o_"+position);
                                        tempImg.setAttribute("src", playerImage);
                                        tempImg.setAttribute("height", closeHeight);
                                        tempImg.setAttribute("width", closeWidth);
                                        tempImg.setAttribute("class", "mazeBlock invisible");
                                        tempImg.setAttribute("rows", rows);
                                        tempImg.setAttribute("cols", cols);
                                        position++;
                                    }
                                    tempTd.appendChild(tempImg);
                                    tempTr.appendChild(tempTd);
                                }
                                game.appendChild(tempTr);
                            }

                            div.setAttribute("class", "container");
                            div.appendChild(game);
                            thirdData.appendChild(div);
                            firstRow.appendChild(thirdData);
                            /////////////////

                            main.appendChild(firstRow);
                            document.body.appendChild(main);
                            // the update button
                            var updateButton;
                            updateButton = document.createElement("button");
                            updateButton.setAttribute("id", "update");
                            updateButton.setAttribute("name", "update");
                            $("#update").css("display","none");
                            document.body.appendChild(updateButton);
                            // the update of the opponent maze button
                            var updateOpponentButton;
                            updateOpponentButton = document.createElement("button");
                            updateOpponentButton.setAttribute("id", "updateOpponent");
                            updateOpponentButton.setAttribute("name", "updateOpponent");
                            $("#updateOpponent").css("display","none");
                            document.body.appendChild(updateOpponentButton);
                        move_long_polling();
                    });
                }
                long_polling();
            });
        </script>




    </body>
</html>
