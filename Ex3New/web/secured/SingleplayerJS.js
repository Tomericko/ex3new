/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var currentClue = 0;
//for singleplayer game
$(document).on('click',"#clueBtn",function(){
    var position = document.getElementById("player").childNodes.item(0).id;
    $.getJSON('SingleplayerBtnServlet', {type: "clue", clueBtn: position.toString(),gameType: "single"}, function (data) {
        var clue = data.newClue;
        var direction = data.direction;
        document.getElementById(clue).setAttribute("src", "resources/maze/arrow_"+direction+".png");
        $("#" + clue).css("display", "block");
        currentClue = clue;
    });
});


$(document).on('click', "#start_overBtn", function () {
    if(confirm("Are you sure?")){
        $.getJSON('SingleplayerBtnServlet', {type: "start_over", gameType: "single"}, function (data) {
            var position = data.Position;
            
            var cur = $("#player").children("img").attr("id"); //return the player position
            if(cur!==position){
                document.getElementById(String(position)).src = document.getElementById(String(cur)).src;
                $("#" + String(position)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(position)).parent().attr("id", "player");
                $("#clueBtm").attr("value",position);
            }
        
        });
    }
});


$(document).on('click', "#exitBtn", function () {
    if(confirm("Are you sure?")){  
        window.history.back();
    }
});


$(function () {
    $("#main").focus();

    $("#main").focusout(function () {
        $("#main").focus();
    });

});


$(function () {
    $("html").keyup(function (event) {
        var key = event.which;

        var cur = $("#player").children("img").attr("id"); //return the player position
        //var rows = $("#player").children("img").attr("rows");
        var cols = $("#player").children("img").attr("cols");
        if (parseInt(String(key)) === 38) {
            //up
            var up = parseInt(cur) - (2 * parseInt(cols));
            var middle = parseInt(cur) - parseInt(cols);
            if ($("#" + String(up)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(up)).src = document.getElementById(String(cur)).src;
                $("#" + String(up)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(up)).parent().attr("id", "player");
                $("#clueBtm").attr("value",up);

            } else if ($("#" + String(up)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(up, cur);
            }
        } else if (parseInt(String(key)) === 39) {
            //right
            var right = 2 + parseInt(cur);
            var middle = 1 + parseInt(cur);
            //$("#main").val($("#"+String(right)));
            if ($("#" + String(right)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(right)).src = document.getElementById(String(cur)).src;
                $("#" + String(right)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(right)).parent().attr("id", "player");
                $("#clueBtm").attr("value",right);

            } else if ($("#" + String(right)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(right, cur);
            }
        } else if (parseInt(String(key)) === 37) {
            //left
            var left = (-2) + parseInt(cur);
            var middle = (-1) + parseInt(cur);
            if ($("#" + String(left)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(left)).src = document.getElementById(String(cur)).src;
                $("#" + String(left)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(left)).parent().attr("id", "player");
                $("#clueBtm").attr("value",left);

            } else if ($("#" + String(left)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(left, cur);
            }
        } else if (parseInt(String(key)) === 40) {
            //down
            var down = parseInt(cur) + 2 * parseInt(cols);
            var middle = parseInt(cur) + parseInt(cols);
            $("#main").val(String(key));
            if ($("#" + String(down)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(down)).src = document.getElementById(String(cur)).src;
                $("#" + String(down)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(down)).parent().attr("id", "player");
                $("#clueBtm").attr("value",down);

            } else if ($("#" + String(down)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(down, cur);
            }
        }
        $("#" + currentClue).css("display", "none");
    });
});




function win(direction, cur) {
    //change image
    document.getElementById(String(direction)).src = document.getElementById(String(cur)).src;
    $("#" + String(direction)).css("display", "block");
    $("#" + String(cur)).css("display", "none");

    //update td kind = id
    $("#player").attr("id", "open");

    if (confirm("Good Job! Again? ")) {
        location.reload();
    } else {
        document.getElementById("winBtn").submit();
    }
}


String.prototype.replaceAt = function (index, character) {
    return this.substr(0, index) + character + this.substr(index + character.length);
};


