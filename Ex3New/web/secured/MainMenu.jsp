<%-- 
    Document   : MainMenu
    Created on : 23/05/2016, 09:33:05
    Author     : tomer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="MainMenuCSS.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="MainMenuJS.js"></script>
        <title>Help Mr.Potato</title>
    </head>
    <body>
        <div id="toolBar">
            <label style="font-size: 1.5vw;">Welcome <%=(String) request.getSession().getAttribute("username")%>!</label><br/>
            <img id="user_icon" src="<%=(String) request.getSession().getAttribute("icon")%>" align="middle"><br/>
            <a href="/secured/LogoutServlet" onclick="logout()" style="font-size: 1.5vw;" name="logout" id="logout_btn" style="font-size: 100%">Logout</a>
        </div>
        <div id="mainDiv">
            <img id="backgroundImg"src="resources/mainmenu/background.jpg" id="mainFrame">
            <form action="../secured/MainMenuServlet" method="post">
                <input type="text" class="txtBox" placeholder="maze" name="mazeName"><br/>
                <input type="submit" value="" class="button" id="singleplayer_btn" name="singleplayer_btn"/><br/>
                <input type="submit" value="" class="button" id="multiplayer_btn" name="multiplayer_btn"/>
            </form>
        </div>
    </body>
</html>