<%@page import="mainPackage.Maze"%>
<html>
    <head>
        <title>Singleplayer</title>     
        <script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
        <script src="SingleplayerJS.js"></script>
        <link rel="stylesheet" type="text/css" href="SingleplayerCSS.css">
    </head>
    <body>
        <%request.getSession().setAttribute("type", "singleplayer");%>
        <div id="toolBar">
            <label style="font-size: 1.5vw;">Welcome <%=(String) request.getSession().getAttribute("username")%>!</label><br/>
            <img id="user_icon" src="<%=(String) request.getSession().getAttribute("icon")%>" align="middle"><br/>
            <a href="/secured/LogoutServlet" onclick="logout()" style="font-size: 1.5vw;" name="logout" id="logout_btn" style="font-size: 100%">Logout</a>
           

        </div>
        <table>
            <tr>
                <td><button id="clueBtn" name="clueBtn" title="hint"></button></td>
                <td> <button id="start_overBtn" name="start_overBtn" title="start over"></button></td>
                <td><button id="exitBtn" name="exitBtn" title="escape"></button></td>
            </tr>
        </table>
        <img src="resources/maze/mr_potato_head_heavy.gif" alt="Loading..." id="loadingImg" class="center">

        <script type="text/javascript">
            $(function ($) {
                function long_polling() {
                    $.getJSON('SinglePlayerLongPolling', function (data) {
                        $("#loadingImg").css("display", "none");

                        var rows = data.answer.height * 2 - 1;
                        var cols = data.answer.width * 2 - 1;
                        var maze = data.answer.maze.mazeString.replaceAt((data.answer.maze.entrance.row * 2) * (rows) + data.answer.maze.entrance.col * 2, '2');
                        maze = maze.replaceAt((data.answer.maze.end.row * 2) * (rows) + data.answer.maze.end.col * 2, '3');

                        var blockImage = "resources/maze/Brick_Block.png";
                        var borderImage = "resources/maze/Used_Block.png";
                        var playerImage = "resources/maze/player.png";
                        var exitImage = "resources/maze/exit5.png";
                        var playerHeight = 500.0 / rows + 2;
                        var playerWidth = 850.0 / cols + 2;
                        var closeHeight = 600.0 / rows + 2;
                        var closeWidth = 850.0 / cols + 2;
                        var borderHeight = 600.0 / rows + 2;
                        var borderWidth = 850.0 / cols + 2;

                        var main = document.createElement("table");
                        main.setAttribute("id", "mainTable");
                        var firstRow = document.createElement("tr");
                        var firstData = document.createElement("td");

                        var secondData = document.createElement("td");
                        var div = document.createElement("div");
                        var game = document.createElement("table");
                        game.setAttribute("id", "gameBoard");

                        var i = 0, j = 0;
                        var position = 0;
                        var tempImg;
                        var tempTd;
                        var tempTr;
                        for (i = 0; i < rows + 2; i++) {
                            tempTr = document.createElement("tr");

                            for (j = 0; j < cols + 2; j++) {
                                tempTd = document.createElement("td");
                                tempImg = document.createElement("img");
                                if (i === 0 || i === cols + 1 || j === 0 || j === rows + 1) {
                                    tempImg.setAttribute("id", "close");
                                    tempImg.setAttribute("src", borderImage);
                                    tempImg.setAttribute("height", borderHeight);
                                    tempImg.setAttribute("width", borderWidth);
                                    tempImg.setAttribute("class", "mazeBlock");
                                } else if (maze[position] === '1') {
                                    tempTd.setAttribute("id", "border");
                                    tempImg.setAttribute("id", position);
                                    tempImg.setAttribute("src", blockImage);
                                    tempImg.setAttribute("height", closeHeight);
                                    tempImg.setAttribute("width", closeWidth);
                                    tempImg.setAttribute("class", "mazeBlock");
                                    tempImg.setAttribute("rows", rows);
                                    tempImg.setAttribute("cols", cols);
                                    position++;
                                } else if (maze[position] === '2') {
                                    tempTd.setAttribute("id", "player");
                                    tempImg.setAttribute("id", position);
                                    tempImg.setAttribute("src", playerImage);
                                    tempImg.setAttribute("height", playerHeight);
                                    tempImg.setAttribute("width", playerWidth);
                                    tempImg.setAttribute("class", "mazeBlock");
                                    tempImg.setAttribute("rows", rows);
                                    tempImg.setAttribute("cols", cols);
                                    $("#clueBtn").attr("value", position);
                                    position++;
                                } else if (maze[position] === '3') {
                                    tempTd.setAttribute("id", "end");
                                    tempImg.setAttribute("id", position);
                                    tempImg.setAttribute("src", exitImage);
                                    tempImg.setAttribute("height", closeHeight);
                                    tempImg.setAttribute("width", closeWidth);
                                    tempImg.setAttribute("class", "mazeBlock");
                                    tempImg.setAttribute("rows", rows);
                                    tempImg.setAttribute("cols", cols);
                                    position++;
                                } else if (maze[position] === '0') {
                                    tempTd.setAttribute("id", "open");
                                    tempImg.setAttribute("id", position);
                                    tempImg.setAttribute("src", playerImage);
                                    tempImg.setAttribute("height", closeHeight);
                                    tempImg.setAttribute("width", closeWidth);
                                    tempImg.setAttribute("class", "mazeBlock invisible");
                                    tempImg.setAttribute("rows", rows);
                                    tempImg.setAttribute("cols", cols);
                                    position++;
                                }
                                tempTd.appendChild(tempImg);
                                tempTr.appendChild(tempTd);
                            }
                            game.appendChild(tempTr);
                        }

                        div.setAttribute("class", "container");
                        div.appendChild(game);
                        secondData.appendChild(div);
                        firstRow.appendChild(secondData);
                        main.appendChild(firstRow);
                        document.body.appendChild(main);

                    });
                }
                long_polling();
            });
        </script>
    </body>
</html>