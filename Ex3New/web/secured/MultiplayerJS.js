/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var currentClue = 0;
var direction = "";
//for multiplayer game
$(document).on('click', "#clueBtnMulti", function () {
    var position = document.getElementById("player").childNodes.item(0).id;
    $.getJSON('SingleplayerBtnServlet', {type: "clue", clueBtn: position.toString(), gameType: "multi"}, function (data) {
        var clue = data.newClue;
        var direction = data.direction;
        document.getElementById(clue).setAttribute("src", "resources/maze/arrow_" + direction + ".png");
        $("#" + clue).css("display", "block");
        currentClue = clue;
    });
});

$(document).on('click', "#exitBtn", function () {
    closeAndBack();
});

$(document).on('click', "#start_overBtn", function () {
    closeAndBack();
});

function closeAndBack(){
    if(confirm("Are you sure you want to exit?")){
        $.getJSON('SingleplayerBtnServlet', {type: "close", gameType: "multi"}, function (data) {  
        });
        window.history.back();
    }
}


$(function () {
    $("#main").focus();

    $("#main").focusout(function () {
        $("#main").focus();
    });

});



$(document).on('click', "#update", function () {
    $.getJSON('UpdateOpponent', {direction: direction.toString()}, function () {

    });
});




$(function () {
    $("html").keyup(function (event) {
        var key = event.which;

        var cur = $("#player").children("img").attr("id"); //return the player position
        var cols = $("#player").children("img").attr("cols");
        if (parseInt(String(key)) === 38) {
            //up
            var up = parseInt(cur) - (2 * parseInt(cols));
            var middle = parseInt(cur) - parseInt(cols);

            if ($("#" + String(up)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(up)).src = document.getElementById(String(cur)).src;
                $("#" + String(up)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(up)).parent().attr("id", "player");
                $("#clueBtmMulti").attr("value", up);
                direction = "up";
                $("#update").click();
            } else if ($("#" + String(up)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(up, cur);
            }
        } else if (parseInt(String(key)) === 39) {
            //right
            var right = 2 + parseInt(cur);
            var middle = 1 + parseInt(cur);
            //$("#main").val($("#"+String(right)));
            if ($("#" + String(right)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(right)).src = document.getElementById(String(cur)).src;
                $("#" + String(right)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(right)).parent().attr("id", "player");
                $("#clueBtmMulti").attr("value", right);
                direction = "right";
                $("#update").click();

            } else if ($("#" + String(right)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(right, cur);
            }
        } else if (parseInt(String(key)) === 37) {
            //left
            var left = (-2) + parseInt(cur);
            var middle = (-1) + parseInt(cur);
            if ($("#" + String(left)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(left)).src = document.getElementById(String(cur)).src;
                $("#" + String(left)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(left)).parent().attr("id", "player");
                $("#clueBtmMulti").attr("value", left);
                direction = "left";
                $("#update").click();

            } else if ($("#" + String(left)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(left, cur);
            }
        } else if (parseInt(String(key)) === 40) {
            //down
            var down = parseInt(cur) + 2 * parseInt(cols);
            var middle = parseInt(cur) + parseInt(cols);
            $("#main").val(String(key));
            if ($("#" + String(down)).parent().attr("id") === "open" && $("#" + String(middle)).parent().attr("id") === "open") {
                //change image
                document.getElementById(String(down)).src = document.getElementById(String(cur)).src;
                $("#" + String(down)).css("display", "block");
                $("#" + String(cur)).css("display", "none");

                //update td kind = id
                $("#player").attr("id", "open");
                $("#" + String(down)).parent().attr("id", "player");
                $("#clueBtmMulti").attr("value", down);
                direction = "down";
                $("#update").click();

            } else if ($("#" + String(down)).parent().attr("id") === "end" && $("#" + String(middle)).parent().attr("id") === "open") {
                win(down, cur);
            }
        }
        $("#" + currentClue).css("display", "none");
    });
});




function win(direction, cur) {
    //change image
    document.getElementById(String(direction)).src = document.getElementById(String(cur)).src;
    $("#" + String(direction)).css("display", "block");
    $("#" + String(cur)).css("display", "none");

    //update td kind = id
    $("#player").attr("id", "open");

    if (confirm("Good Job! Again? ")) {
        location.reload();
    } else {
        document.getElementById("winBtn").submit();
    }
}


String.prototype.replaceAt = function (index, character) {
    return this.substr(0, index) + character + this.substr(index + character.length);
};

var to1 = "";
$(document).on('click', "#updateOpponent", function () {
    var to = to1;
    //var to = $("#updateOpponent").attr("value");
    var cur = $("#opPlayer").children("img").attr("id"); //return the player position
    var cols = $("#opPlayer").children("img").attr("cols");
    cur = cur.slice(2);
    if ("up" === String(to)) {
        //up
        var up = parseInt(cur) - (2 * parseInt(cols));
        var middle = parseInt(cur) - parseInt(cols);

        if ($("#o_" + String(up)).parent().attr("id") === "open" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            //change image
            document.getElementById(String(up)).src = document.getElementById(String(cur)).src;
            $("#o_" + String(up)).css("display", "block");
            $("#o_" + String(cur)).css("display", "none");

            //update td kind = id
            $("#opPlayer").attr("id", "open");
            $("#o_" + String(up)).parent().attr("id", "opPlayer");
        } else if ($("#o_" + String(up)).parent().attr("id") === "end" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            win(up, cur);
        }
    } else if ("right" === String(to)) {
        //right

        var right = 2 + parseInt(cur);
        var middle = 1 + parseInt(cur);
        //$("#main").val($("#"+String(right)));

        if ($("#o_" + String(right)).parent().attr("id") === "open" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            //change image


            document.getElementById(String(right)).src = document.getElementById(String(cur)).src;
            $("#o_" + String(right)).css("display", "block");
            $("#o_" + String(cur)).css("display", "none");

            //update td kind = id
            $("#opPlayer").attr("id", "open");
            $("#o_" + String(right)).parent().attr("id", "opPlayer");

        } else if ($("#o_" + String(right)).parent().attr("id") === "end" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            win(right, cur);
        }
    } else if ("left" === String(to)) {
        //left
        var left = (-2) + parseInt(cur);
        var middle = (-1) + parseInt(cur);

        if ($("#o_" + String(left)).parent().attr("id") === "open" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            //change image


            document.getElementById(String(left)).src = document.getElementById(String(cur)).src;
            $("#o_" + String(left)).css("display", "block");
            $("#o_" + String(cur)).css("display", "none");

            //update td kind = id
            $("#opPlayer").attr("id", "open");
            $("#o_" + String(left)).parent().attr("id", "opPlayer");

        } else if ($("#o_" + String(left)).parent().attr("id") === "end" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            win(left, cur);
        }
    } else if ("down" === String(to)) {
        //down
        var down = parseInt(cur) + 2 * parseInt(cols);
        var middle = parseInt(cur) + parseInt(cols);
        //$("#main").val(String(key));
        if ($("#o_" + String(down)).parent().attr("id") === "open" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            //change image

            document.getElementById(String(down)).src = document.getElementById(String(cur)).src;

            $("#o_" + String(down)).css("display", "block");
            $("#o_" + String(cur)).css("display", "none");

            //update td kind = id
            $("#opPlayer").attr("id", "open");
            $("#o_" + String(down)).parent().attr("id", "opPlayer");

        } else if ($("#o_" + String(down)).parent().attr("id") === "end" && $("#o_" + String(middle)).parent().attr("id") === "open") {
            win(down, cur);
        }

    }
    $("#" + currentClue).css("display", "none");
});


function move_long_polling() {
    $.getJSON('MoveLongPolling', function (data) {
        to1 =  data.answer.Move;
        if(to1 === "close"){
            alert("It seems like your opponent disconected.\n\n\
                    Technical win to you! :)");
                window.history.back();
        }else{
            $("#updateOpponent").click();
            move_long_polling();
        }

    });
}


