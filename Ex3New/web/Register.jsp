<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="RegisterCSS.css">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="RegisterJS.js"></script>
        <title>Register</title>
    </head>
    <body>
        <%if (request.getAttribute("error") != null && (Boolean) request.getAttribute("error")) {%>
        <script>alert("Username already exist");</script>
        <%}%>
        <div id="mainDiv">
            <img id="backgroundImg" src="resources/signup_background.jpg" id="mainFrame">
            <form action="CreateAccountServlet" method="post" id="mainForm" >
                <input type="text" name="username" id="username" class="txtBox" placeholder="username">
                <input type="password" name="password" id="password" class="txtBox" placeholder="password"><br/>
                <input type="text" name="real_name" id="real_name" class="txtBox" placeholder="name">
                <input type="email" name="email" id="email" class="txtBox" placeholder="email"><br/>
                <select id="icons" class="txtBox" style="margin-top: 3px;">
                    <option value="Woody">Woody</option>
                    <option value="Buzz" >Buzz</option>
                    <option value="Slinky">Slinky</option>
                    <option value="Jessie">Jessie</option>
                </select>
                <img id="icon_choose" src="resources/icons/Woody_icon.png" height="20%" width="20%" align="middle"><br/>
                <input type="text" hidden="true" name="user_icon" id="user_icon" value="resources/icons/Woody_icon.png"><br/>  
                <input type="image" alt="Submit" hidden="true" value="" name="regiBtn" id="regiBtn">
                <input type="button" onclick="check_all()" class="button" value="" name="register" id="register_btn">
                <input  onclick="reset()" type="button" class="button"  value="" name="registerReset" id="reset_btn"><br/>
            </form>
        </div>
    </body>
</html>