<%@page import="java.text.SimpleDateFormat"%> 
<%@page import="java.util.Calendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8" session="false"%>
<!DOCTYPE html> 
<html>  
    <head>   
        <link rel="stylesheet" type="text/css" href="LoginCSS.css">
        <title>Login</title>     
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="LoginJS.js"></script>
    </head>  
    <body>
        <div id="mainDiv">
            <img id="backgroundImg" src="resources/login_background.jpg" id="mainFrame">
            <form action="MyFormServlet" method="post">
                <input type="text" class="txtBox" name="username" placeholder="ursername"/><br/>
                <input type="password" class="txtBox" name="password" placeholder="password"/><br/>
                <input type="submit" class="button" name="login" id="login_btn" value=""/><br/>
                <input type="submit" class="button" name="signup"  id="signup_btn" value=""/>
            </form>
        </div>
    </body>
</html>