
import objects.Record;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "MyPrivateData", urlPatterns = {"/secured/MyPrivateData"})
public class MyPrivateData extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Record> list = new ArrayList<Record>();
        list.add(new Record(100, "Lunch", false));
        list.add(new Record(5000, "Salary", true));
        list.add(new Record(300, "Gift", true));
        request.getSession().setAttribute("list", list);
        request.getRequestDispatcher("data.jsp").forward(request, response);
    }
}
