/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import objects.Maze;
import objects.Position;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tomer
 */
@WebServlet(urlPatterns = {"/SingleplayerServlet"})
public class SingleplayerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        Maze maze  = new Maze();
        Position end = new Position();
        Position entrance = new Position();
        end.setCol(new Integer(3));
        end.setRow(new Integer(9));
        entrance.setCol(new Integer(0));
        entrance.setRow(new Integer(13));
        maze.setEnd(end);
        maze.setEntrance(entrance);
        maze.setMazeName("NameOfTheMaze");
        maze.setMazeString("0000000000010000010100010000011111111110111110101110101010000100000000000000010000010101101011101111111110111110101001000100010000000101000001010011111110101011101011111110100000000001010100000000000001001111111111111011111110101010010000000001010100000001010101111111111010111011111010101000010000000000000100000101010110111111111010111011101011100100000000000101000100010001001110101111101110101111101010000001010000010001010000010100111010111010111011111011111001000100010101000100000000010010101010111110111010101011100101010100010001000101010001001110101011101111101111101111010001010100010000010000000000101010111111101111111110111001010101010000010001000000010011101110111111101010101010100101010000000000010001010101001011111010111011111111111010000000010101000100000001000100111010111110101111111011111001000101000001010000000000000");
        int height = 15;
        int width = 15;
        
        request.getSession(false).setAttribute("maze", maze);
        request.getSession(false).setAttribute("height", height);
        request.getSession(false).setAttribute("width", width);
                
        response.sendRedirect("Singleplayer.jsp");

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
