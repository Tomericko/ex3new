package objects;


public class Record {

    private double value;
    private String desc;
    private boolean deposite;

    public Record(double value, String desc, boolean deposite) {
        this.deposite = deposite;
        this.value = value;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public double getValue() {
        return value;
    }

    public boolean isDeposite() {
        return deposite;
    }
}
