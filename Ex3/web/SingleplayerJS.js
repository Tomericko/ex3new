/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(function(){
   $("#main").focus();
   
   $("#main").focusout(function(){
      $("#main").focus(); 
   });
    
});


$(function(){
    $("html").keyup(function(event){
        var key = event.which;
        
        var cur = $("#player").children("img").attr("id"); //return the player position
        //var rows = $("#player").children("img").attr("rows");
        var cols = $("#player").children("img").attr("cols");
        if(parseInt(String(key))  === 38){
            //up
            var up = parseInt(cur) - (2*parseInt(cols));
            var middle = parseInt(cur) - parseInt(cols);
            if($("#"+String(up)).parent().attr("id") === "open" && $("#"+String(middle)).parent().attr("id") === "open"){
                //change image
                document.getElementById(String(up)).src =  document.getElementById(String(cur)).src;
                $("#"+String(up)).css("display","block");
                $("#"+String(cur)).css("display","none");
                
                //update td kind = id
                $("#player").attr("id","open");
                $("#"+String(up)).parent().attr("id","player");

            }else if($("#"+String(up)).parent().attr("id") === "end" && $("#"+String(middle)).parent().attr("id") === "open"){
                win(up,cur);
            }
        }else if(parseInt(String(key))===39){
            //right
            var right = 2 + parseInt(cur);
            var middle = 1 + parseInt(cur);
            //$("#main").val($("#"+String(right)));
            if($("#"+String(right)).parent().attr("id") === "open" && $("#"+String(middle)).parent().attr("id") === "open"){
                //change image
                document.getElementById(String(right)).src =  document.getElementById(String(cur)).src;
                $("#"+String(right)).css("display","block");
                $("#"+String(cur)).css("display","none");
                
                //update td kind = id
                $("#player").attr("id","open");
                $("#"+String(right)).parent().attr("id","player");
                
            }else if($("#"+String(right)).parent().attr("id") === "end" && $("#"+String(middle)).parent().attr("id") === "open"){
                win(right,cur);
            }
        }else if(parseInt(String(key))===37){
            //left
            var left = (-2) + parseInt(cur);
            var middle = (-1) + parseInt(cur);
            if($("#"+String(left)).parent().attr("id") === "open" && $("#"+String(middle)).parent().attr("id") === "open"){
                //change image
                document.getElementById(String(left)).src =  document.getElementById(String(cur)).src;
                $("#"+String(left)).css("display","block");
                $("#"+String(cur)).css("display","none");
                
                //update td kind = id
                $("#player").attr("id","open");
                $("#"+String(left)).parent().attr("id","player");
                
            }else if($("#"+String(left)).parent().attr("id") === "end" && $("#"+String(middle)).parent().attr("id") === "open"){
                win(left,cur);
            }
        }else if(parseInt(String(key)) === 40){
            //down
            var down = parseInt(cur) + 2*parseInt(cols);
            var middle = parseInt(cur) + parseInt(cols);
            $("#main").val(String(key));
            if($("#"+String(down)).parent().attr("id") === "open" && $("#"+String(middle)).parent().attr("id") === "open"){
                //change image
                document.getElementById(String(down)).src =  document.getElementById(String(cur)).src;
                $("#"+String(down)).css("display","block");
                $("#"+String(cur)).css("display","none");
                
                //update td kind = id
                $("#player").attr("id","open");
                $("#"+String(down)).parent().attr("id","player");

            }else if($("#"+String(down)).parent().attr("id") === "end" && $("#"+String(middle)).parent().attr("id") === "open"){
                win(down,cur);
            }
        }
    });
});
    
    
    
function win(direction, cur){
        //change image
        document.getElementById(String(direction)).src =  document.getElementById(String(cur)).src;
        $("#"+String(direction)).css("display","block");
        $("#"+String(cur)).css("display","none");

        //update td kind = id
        $("#player").attr("id","open");

        if(confirm("Good Job! Again? ")){
            location.reload();
        }else{
            document.getElementById("winBtn").submit();
        }
}