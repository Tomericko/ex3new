
function styleBtn(pic_id,pic){
       $(pic_id).css("background","url("+'"'+"resources/"+pic+".jpg"+'"'+")");
       $(pic_id).css("border","0");
       $(pic_id).css("background-repeat","no-repeat");
       $(pic_id).css("background-size","contain");
       $(pic_id).css("height","25px");
       $(pic_id).css("width","150px");
}

$(function(){
   $("#login_btn").hover(function(){
        styleBtn("#login_btn","btn_login2");
   },function(){
        styleBtn("#login_btn","btn_login1");
   }); 
});

$(function(){
   $("#login_btn").mousedown(function(){
        styleBtn("#login_btn","btn_login3");
   });
});

$(function(){
   $("#login_btn").mouseup(function(){
       styleBtn("#login_btn","btn_login1");
   });
});

$(function(){
   $("#signup_btn").hover(function(){
      styleBtn("#signup_btn","btn_signup2");
   },function(){
      styleBtn("#signup_btn","btn_signup1");
   }); 
});

$(function(){
   $("#signup_btn").mousedown(function(){
      styleBtn("#signup_btn","btn_signup3");
   });
});

$(function(){
   $("#signup_btn").mouseup(function(){
     styleBtn("#signup_btn","btn_signup1");
   });
});

$(".user").focusin(function(){
  $(".inputUserIcon").css("color", "#e74c3c");
}).focusout(function(){
  $(".inputUserIcon").css("color", "white");
});

$(".pass").focusin(function(){
  $(".inputPassIcon").css("color", "#e74c3c");
}).focusout(function(){
  $(".inputPassIcon").css("color", "white");
});
