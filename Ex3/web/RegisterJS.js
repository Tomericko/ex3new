/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function change_icon(icon){
    document.getElementById("icon_choose").src = "resources/icons/"+icon.toString()+"_icon.png";
}

$(function(){
    $("#icons").change(function(){
        change_icon($("#icons").val());
    });
});
    

function styleBtn(pic_id,pic){
       $(pic_id).css("src","url("+'"'+"resources/register/"+pic+".jpg"+'"'+")");
       $(pic_id).css("border","0");
       $(pic_id).css("height","70px");
       $(pic_id).css("width","150px");
       $(pic_id).css("display","block");
}

$(function(){
   $("#register_btn").hover(function(){
        styleBtn("#register_btn","btn_register2");
   },function(){
        styleBtn("#register_btn","btn_register1");
   }); 
});

$(function(){
   $("#register_btn").mousedown(function(){
        styleBtn("#register_btn","btn_register3");
   });
});

$(function(){
   $("#register_btn").mouseup(function(){
       styleBtn("#register_btn","btn_register1");
   });
});

$(function(){
   $("#reset_btn").hover(function(){
      styleBtn("#reset_btn","btn_reset2");
   },function(){
      styleBtn("#reset_btn","btn_reset1");
   }); 
});

$(function(){
   $("#reset_btn").mousedown(function(){
      styleBtn("#reset_btn","btn_reset3");
   });
});

$(function(){
   $("#reset_btn").mouseup(function(){
     styleBtn("#reset_btn","btn_reset1");
   });
});

$(".username").focusin(function(){
  $(".inputUserIcon").css("color", "#e74c3c");
}).focusout(function(){
  $(".inputUserIcon").css("color", "white");
});

$(".pass").focusin(function(){
  $(".inputPassIcon").css("color", "#e74c3c");
}).focusout(function(){
  $(".inputPassIcon").css("color", "white");
});
