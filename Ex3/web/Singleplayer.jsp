<%@page import="objects.Maze" %>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="SingleplayerCSS.css">
        <title>Singleplayer</title>     
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="SingleplayerJS.js"></script>
    </head>
    <body>
        <form action="WinningServlet" method="post">
            <input  type="button" id="winBtn">
        </form>
        <%Maze maze = null;%>
        <%maze = (Maze)request.getSession().getAttribute("maze");%>
        <%StringBuilder mazeString =new StringBuilder(maze.getMazeString());%>
        <%Integer mazeHeight = (Integer)request.getSession().getAttribute("height");%>
        <%Integer mazeWidth = (Integer)request.getSession().getAttribute("width");%>
        <%int rows = mazeHeight*2 - 1;%>
        <%int cols = mazeWidth*2 - 1;%>
        <%mazeString.setCharAt((( maze.getEntrance().getRow()*2) * (cols-2) + maze.getEntrance().getCol()*2), '2');
        mazeString.setCharAt((( maze.getEnd().getRow()*2) * (cols-2) + maze.getEnd().getCol()*2) , '3'); 
        %>
        <table id="mainTable">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td><img src="resources/maze/btn_exit.png"></td>
                        </tr>
                        <tr>
                            <td><img src="resources/maze/btn_clue.png"></td>
                        </tr>
                        <tr>
                            <td><img src="resources/maze/btn_start_over.png"></td>
                        </tr>
                    </table>
                </td>
                <td></td>
                <td>
                    <table >

                        <div class="container">
                                <table id="mainTable" >
                                    <tr>
                                        <%-- need to change to check if height width and string are ok --%>
                                        <% if(cols!=0 && rows!=0 && mazeString !=null){
                                            int position = 0;
                                            for(int i=0;i<rows+2;i++){%>
                                    <tr>
                                                <%for(int j=0;j<cols+2;j++){%>
                                                    <%if(i == 0 || i==cols+1 || j==0 || j==rows+1){%>
                                                        <td> <img src="resources/maze/block2.png" height="<%=600.0/(double)rows+2%>" rows="<%=rows%>" cols="<%=cols%>" width="<%=850.0/(double)cols+2%>" class="mazeBlock"></td>
                                                    <%continue;}%>
                                                    <%if(mazeString.charAt(position) == '1'){%>
                                                        <td  id="border"> <img id="<%=position%>" src="resources/maze/block1.png" rows="<%=rows%>" cols="<%=cols%>" height="<%=600.0/(double)rows+2%>" width="<%=850.0/(double)cols+2%>" class="mazeBlock"></td>
                                                    <%position++;continue;}%>
                                                    <%if(mazeString.charAt(position) == '0'){%>
                                                        <td id="open" > <img id="<%=position%>" src="resources/maze/block1.png"  rows="<%=rows%>" cols="<%=cols%>" height="<%=600.0/(double)rows+2%>" width="<%=850.0/(double)cols+2%>" class="mazeBlock invisible"></td>
                                                    <%position++;continue;}%>
                                                    <%if(mazeString.charAt(position) == '2'){%>
                                                        <td id="player"> <img id="<%=position%>" src="resources/maze/player.png"  rows="<%=rows%>" cols="<%=cols%>" height="<%=500.0/(double)rows+2%>" width="<%=850.0/(double)cols+2%>" class="mazeBlock"></td>
                                                    <%position++;continue;}%>
                                                    <%if(mazeString.charAt(position) == '3'){%>
                                                        <td id="end" > <img id="<%=position%>" src="resources/maze/exit5.png" rows="<%=rows%>" cols="<%=cols%>" height="<%=600.0/(double)rows+2%>" width="<%=850.0/(double)cols+2%>" class="mazeBlock"></td>
                                                    <%position++;continue;}%>
                                              <% }%>
                                    </tr>
                                            <%}%>
                                        <%}%>
                                    </tr>
                                </table>
                            </div>

                    </tr>
                </table>
            </td>
        </tr>
        
    </body>
</html>
