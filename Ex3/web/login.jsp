<!DOCTYPE html> 
<html>  
    <head>   
        <link rel="stylesheet" type="text/css" href="LoginCSS.css">
        <title>Login</title>     
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="loginJS.js"></script>
    </head>  
    <body>    
        <img src="resources/login_background.jpg" id="mainFrame">
            <form action="MyFormServlet" method="post" class="centered">         
                <div>
                    <input type="text" class="user" name="username" placeholder="ursername"/>
                </div>
                <div>
                    <input type="password" class="pass" name="password" placeholder="password"/>
                </div>
                <table>
                    <tr>
                        <td><input type="submit" name="login" id="login_btn" value=""/></td>
                    </tr>
                    <tr>
                        <td><input type="submit" name="signup"  id="signup_btn" value=""/></td>
                    </tr>
                </table>
            </form>   
        </img>
    </body>
</html>