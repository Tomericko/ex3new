<%-- 
    Document   : MainMenu
    Created on : 23/05/2016, 09:33:05
    Author     : tomer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="MainMenuCSS.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="MainMenuJS.js"></script>
        <title>Help Mr.Potato</title>
    </head>
    <body>
        <img src="resources/background.jpg" id="mainFrame">
            <form action="MainMenuServlet" method="post">
                <table class="centered">
                    <tr>
                        <td><input type="submit" value="" class="button" id="singleplayer_btn"/></td>
                    </tr>
                        <td><input type="submit" value="" class="button" id="multiplayer_btn"/></td>
                    </tr>
                </table>
            </form>
        </img>
    </body>
</html>
