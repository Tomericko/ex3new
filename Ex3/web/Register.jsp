
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="RegisterCSS.css">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script src="RegisterJS.js"></script>
        <title>Register</title>
    </head>
    <body>
        <img src="resources/signup_background.jpg" id="mainFrame">
            <form action="CreateAccountServlet" method="post" class="centered">
                <div><input type="text" name="username" placeholder="username"></div>
                <div><input type="password" name="pass" placeholder="password"></div>
                <div><input type="text" name="real_name" placeholder="name"></div>
                <div><input type="email" name="email" placeholder="email"></div>
                <div>Icon:
                    <select id="icons">
                    <option value="Woody">Woody</option>
                    <option value="Buzz">Buzz</option>
                    <option value="Slinky">Slinky</option>
                    <option value="Jessie">Jessie</option>
                </select>
                </div>
                <img src="resources/icons/Woody_icon.png" id="icon_choose">
                <div>
                    <input type="image" src="resources/register/btn_register1.jpg" alt="Submit"  value="" name="register" id="register_btn" class="btn">
                </div>
                <div>
                    <input type="image" src="resources/register/btn_reset1.jpg"  value="" name="register1eset" id="reset_btn" class="btn" alt="Submit" >
                </div>
            </form>
        </img>
    </body>
</html>
